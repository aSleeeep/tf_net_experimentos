import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Cliente from './components/Cliente.vue'
import Local from './components/Local.vue'
import Producto from './components/Producto.vue'
import Cargo from './components/Cargo.vue'
import Empleado from './components/Empleado.vue'
import Orden from './components/Orden.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path:'/clientes',
      name:'clientes',
      component:Cliente
    },
    {
      path: '/locales',
      name: 'locales',
      component: Local
    },
    {
      path: '/productos',
      name: 'productos',
      component: Producto
    },
    {
      path: '/cargos',
      name: 'cargos',
      component: Cargo
    },
    {
      path: '/empleados',
      name: 'empleados',
      component: Empleado
    },
    {
      path: '/ordenes',
      name: 'ordenes',
      component: Orden
    }
  ]
})

