using System.Collections;
using tfweb.entity;

namespace tfweb.service
{
    public interface IProductoService:IService<Producto>
    {
           IEnumerable fetchProductoByName (string texto);
    }
}