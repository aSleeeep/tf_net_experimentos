using System.Collections.Generic;
using tfweb.entity;
using tfweb.repository.ViewModel;

namespace tfweb.repository
{
    public interface IOrdenRepository: IRepository<Orden>
    {
           IEnumerable<OrdenViewModel> GetAllOrdenes ();
        IEnumerable<DetalleOrdenViewModel> ListarDetalles (int ordenId);
    }
}