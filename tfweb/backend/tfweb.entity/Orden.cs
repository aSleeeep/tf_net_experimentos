using System;
using System.Collections.Generic;

 using System.ComponentModel.DataAnnotations;
     

namespace tfweb.entity
{
    public class Orden
    {
//--PK
        public int Id { get; set; }

  [Required]
        public string OrdenNro { get; set; }
//--FK
public string PagoMetodo { get; set; }
public int Total { get; set; }

        public int empleadoId { get; set; }
        public Empleado Empleado { get; set; }
//--FK
         public int ClienteId { get; set; }
        public Cliente Cliente { get; set; }
//--

       public IEnumerable<DetalleOrden> DetalleOrden{ get; set; }

    }
}