 using System.ComponentModel.DataAnnotations;
    

namespace tfweb.entity
{
    public class Producto
    {

//  PK
        public int Id { get; set; }
// --
      [Required]
        public string Nombre { get; set; }

           [Required]
        public string Tipo { get; set; }
              [Required]
        public int Precio { get; set; }
    }
}