using System.Collections.Generic;
using System.Linq;
using tfweb.entity;
using tfweb.repository.Context;

namespace tfweb.repository.Implementacion
{
    public class ClienteRepository : IClienteRepository
    {
         private ApplicationDbContext context;

        public ClienteRepository(ApplicationDbContext context)
        {
            this.context = context;
        }






        public bool Delete(int id)
        {
            try
            {
            var resutl = context.Clientes.Single(x => x.Id == id);
            context.Remove(resutl);
            context.SaveChanges();
            }
            catch (System.Exception)
            {
            return false;
            }
            return true;
        }

        public Cliente Get(int id)
        {
           var result = new Cliente();

            try
            {
            result = context.Clientes.Single(x => x.Id == id);
            }
            catch (System.Exception)
            {

            throw;
            }
            return result;
        }


        public IEnumerable<Cliente> GetAll()
        {
            var result = new List<Cliente>();
            try
            {
                result = context.Clientes.ToList();
            }

            catch (System.Exception)
            {

                throw;
            }
            return result;
        }

        public bool Save(Cliente entity)
        {
             try
            {
                context.Add(entity);
                context.SaveChanges();

            }
            catch (System.Exception)
            {

                return false;
            }
            return true;
        }

        public bool Update(Cliente entity)
        {
             try
            {
                 var pro = 

context.Clientes.Single( x => x.Id == entity.Id);

                 pro.Nombre=entity.Nombre;

                 context.Update(pro);
                 context.SaveChanges();
            }
            catch (System.Exception)
            {
                
                return false;
            }
            return true;
        }
    }
}