using System.ComponentModel.DataAnnotations;
using System;
using System.Text;
namespace tfweb.entity
{
    public class Cargo
    {

// PK
        public int Id { get; set; }
// -- 

        [Required]
       
        public string Nombre { get; set; }
        [Required]
        public decimal  Sueldo { get; set; }
    }
}