using System.Collections;
using tfweb.entity;

namespace tfweb.repository
{
    public interface IProductoRepository:IRepository<Producto>
    {
          IEnumerable fetchProductoByName (string texto);
    }
}