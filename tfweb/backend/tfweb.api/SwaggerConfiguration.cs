namespace webfinal.api
{
    public class SwaggerConfiguration
    {
        
        public const string EndpointDescription = "TF API";

       
        public const string EndpointUrl = "/swagger/v1/swagger.json";

        
        public const string ContactName = "Tom. & Ayme";

        
        public const string ContactUrl = "http://gaaa.com.pe";

       
        public const string DocNameV1 = "v1";

        
        public const string DocInfoTitle = "TF API";

        public const string DocInfoVersion = "v1";
        public const string DocInfoDescription = "TF Api";

    }
}