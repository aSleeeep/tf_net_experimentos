using System.Collections.Generic;
using System.Linq;
using tfweb.entity;
using tfweb.repository.Context;

namespace tfweb.repository.Implementacion
{
    public class LocalRepository : ILocalRepository
    {

          private ApplicationDbContext context;

        public LocalRepository(ApplicationDbContext context)
        {
            this.context = context;
        }


        public bool Delete(int id)
        {
            try
            {
            var resutl = context.Locals.Single(x => x.Id == id);
            context.Remove(resutl);
            context.SaveChanges();
            }
            catch (System.Exception)
            {
            return false;
            }
            return true;
        
        }

        public Local Get(int id)
        {
            var result = new Local();

            try
            {
            result = context.Locals.Single(x => x.Id == id);
            }
            catch (System.Exception)
            {

            throw;
            }
            return result;
        }

        public IEnumerable<Local> GetAll()
        {
           var result = new List<Local>();
            try
            {
                result = context.Locals.ToList();
            }
            catch (System.Exception)
            {

                throw;
            }
            return result;
        }

        public bool Save(Local entity)
        {
           try
            {
                context.Locals.Add(entity);
                context.SaveChanges();

            }
            catch (System.Exception)
            {

                return false;
            }
            return true;
        }

        public bool Update(Local entity)
        {
           try
            {
                 var pro = 

context.Locals.Single( x => x.Id == entity.Id);

                 pro.Nombre=entity.Nombre;
                      pro.Direccion=entity.Direccion;
        
                 pro.Distrito=entity.Distrito;

                 context.Update(pro);
                 context.SaveChanges();
            }
            catch (System.Exception)
            {
                
                return false;
            }
            return true;
        }
    }
}