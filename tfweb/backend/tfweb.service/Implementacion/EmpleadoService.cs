using System.Collections.Generic;
using tfweb.entity;
using tfweb.repository;
using tfweb.repository.ViewModel;

namespace tfweb.service.Implementacion
{
    public class EmpleadoService : IEmpleadoService
    {
          private IEmpleadoRepository pro;

        public EmpleadoService  (IEmpleadoRepository pro) {
            this.pro= pro;
        }






       public bool Delete(int id)
        {
            return pro.Delete(id);
        }
      

         public Empleado Get(int id)
        {
            return pro.Get(id);
        }





public IEnumerable<Empleado> GetAll()
        {
           return  pro.GetAll();
        }
        public bool Save(Empleado  entity)
        {
               return pro.Save(entity);
        }

        public bool Update(Empleado entity)
        {
           return pro.Update(entity);
        }
    }
}