
using Microsoft.AspNetCore.Mvc;
using tfweb.entity;
using tfweb.service;

namespace webfinal.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CargoController : ControllerBase
    {

        private ICargoService pro;

        public CargoController(ICargoService pro)
        {
            this.pro = pro;
        }

        [HttpGet]
        public ActionResult Get()
        {
            return Ok(pro.GetAll());
        }

        [HttpPost]
        public ActionResult Post([FromBody] Cargo clie)
        {
            return Ok( pro.Save(clie) );
        }

        [HttpPut]
        public ActionResult Put([FromBody] Cargo clie)
        {
            return Ok( pro.Update(clie) );
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            return Ok(  pro.Delete(id));
        }

    }
}