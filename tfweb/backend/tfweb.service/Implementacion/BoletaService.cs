using System.Collections.Generic;
using tfweb.entity;
using tfweb.repository;
using tfweb.repository.ViewModel;

namespace tfweb.service.Implementacion
{
    public class OrdenService : IOrdenService
    {

        private IOrdenRepository pro;

        public OrdenService (IOrdenRepository pro) {
            this.pro= pro;
        }
        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public Orden Get(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Orden> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<OrdenViewModel> GetAllOrdenes()
        {
            return pro.GetAllOrdenes ();
        }

        public IEnumerable<DetalleOrdenViewModel> ListarDetalles(int ordenId)
        {
            return pro.ListarDetalles (ordenId);
        }

        public bool Save(Orden entity)
        {
               return pro.Save(entity);
        }

        public bool Update(Orden entity)
        {
            throw new System.NotImplementedException();
        }
    }
}