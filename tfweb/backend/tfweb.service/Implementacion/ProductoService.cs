using System.Collections;
using System.Collections.Generic;
using tfweb.entity;
using tfweb.repository;

namespace tfweb.service.Implementacion
{
    public class ProductoService : IProductoService
    {
         private IProductoRepository pro;

        public ProductoService (IProductoRepository pro) 
        {
            this.pro= pro;
        }



        public bool Delete(int id)
        {
            return pro.Delete(id);
        }

        public IEnumerable fetchProductoByName(string texto)
        {
            return pro.fetchProductoByName(texto);
        }

        public Producto Get(int id)
        {
             return pro.Get(id);
        }

         public IEnumerable<Producto> GetAll()
        {
           return pro.GetAll();
        }

        public bool Save(Producto entity)
        {
               return pro.Save(entity);
        }

        public bool Update(Producto entity)
        {
           return pro.Update(entity);
        }
    }
}