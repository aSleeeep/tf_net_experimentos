
 using System.ComponentModel.DataAnnotations;
namespace tfweb.entity
{
    public class Empleado
    {

// PK
        public int Id { get; set; }

// FK
[Required]
        public int IdLocal { get; set; }

        public Local  Local  { get; set; } 
        [Required]
// FK
        public int IdCargo { get; set; }
        public Cargo Cargo { get; set; }
// -- 
[Required]
        public string Nombre { get; set; }
        [Required]
        public string Apellido { get; set; }
        [Required]
        public string Direccion  { get; set; }
        [Required]
        public string Celular { get; set; }
    }
}