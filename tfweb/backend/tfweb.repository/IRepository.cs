
using System.Collections.Generic;

namespace tfweb.repository
{
    public interface IRepository<T>
    {
        bool  Save(T entity);
        bool  Update(T entity);
        bool  Delete(int id);
        T Get(int id);
        IEnumerable<T> GetAll();

    }
}