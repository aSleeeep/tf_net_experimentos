
using Microsoft.AspNetCore.Mvc;
using tfweb.entity;
using tfweb.service;

namespace webfinal.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdenController : ControllerBase
    {

        private IOrdenService pro;

        public OrdenController(IOrdenService pro)
        {
            this.pro = pro;
        }

        [HttpGet]
        public ActionResult Get()
        {
            return Ok(pro.GetAllOrdenes());
        }

       [HttpGet ("{ordenId}")]
        public ActionResult GetDetalleOrden ([FromRoute] int ordenId) // GET DetalleOrden
         {
            return Ok (  pro.ListarDetalles ( ordenId) );
        }


       [HttpPost]
        public ActionResult CrearBoleta ([FromBody] Orden bole)  // POST CrearOrden
        {
            return Ok (
                pro.Save(bole)
            );
        }

    }
}