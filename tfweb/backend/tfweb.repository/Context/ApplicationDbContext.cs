using Microsoft.EntityFrameworkCore;
using tfweb.entity;

namespace tfweb.repository.Context
{
     public class ApplicationDbContext : DbContext 
    {

public DbSet<Cliente> Clientes { get; set; }
public DbSet<Orden> Ordenes { get; set; }
public DbSet<DetalleOrden> DetalleOrdenes { get; set; }
public DbSet<Producto> Productos { get; set; }
public DbSet<Cargo> Cargos { get; set; }     
public DbSet<Empleado> Empleados { get; set; }
public DbSet<Local> Locals { get; set; }

public ApplicationDbContext (DbContextOptions<ApplicationDbContext> Options) : base (Options) 
{

}     
 


        

        protected override void OnModelCreating (ModelBuilder modelBuilder)
         {
            modelBuilder.Entity<Producto> ()
                .Property (p => p.Nombre)
                .HasColumnName ("Nombres")
                .HasMaxLength (50)
                .IsRequired ();

        }
    }
}