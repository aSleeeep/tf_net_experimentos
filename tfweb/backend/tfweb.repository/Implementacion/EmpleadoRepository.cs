using System.Collections.Generic;
using System.Linq;
using tfweb.entity;
using tfweb.repository.Context;

namespace tfweb.repository.Implementacion
{
    public class EmpleadoRepository : IEmpleadoRepository
    {

         private ApplicationDbContext context;

        public EmpleadoRepository(ApplicationDbContext context)
        {
            this.context = context;
        }


        public bool Delete(int id)
        {
             try
            {
            var resutl = context.Empleados.Single(x => x.Id == id);
            context.Remove(resutl);
            context.SaveChanges();
            }
            catch (System.Exception)
            {
            return false;
            }
            return true;
        
        }

        public Empleado Get(int id)
        {
             var result = new Empleado();

            try
            {
            result = context.Empleados.Single(x => x.Id == id);
            }
            catch (System.Exception)
            {

            throw;
            }
            return result;
        }

        public IEnumerable<Empleado> GetAll()
        {
            var result = new List<Empleado>();
            try
            {
                result = context.Empleados.ToList();
            }
            catch (System.Exception)
            {
                throw;
            }
            return result;
        }

        public bool Save(Empleado entity)
        {
            try
            {
                 context.Add(entity);

                if(entity.IdLocal==1 )
                {
               
                  context.SaveChanges();
                }
              

            }
            catch (System.Exception)
            {

                return false;
            }
            return true;
        }

        public bool Update(Empleado entity)
        {
          try
            {
                 var pro = 

context.Empleados.Single( x => x.Id == entity.Id);

                 pro.IdLocal=entity.IdLocal;
                 pro.IdCargo=entity.IdCargo;
                 pro.Nombre=entity.Nombre;
                 pro.Apellido=entity.Apellido;
                 pro.Direccion=entity.Direccion;
                 pro.Celular=entity.Celular;
                 
                 context.Update(pro);
                 context.SaveChanges();
            }
            catch (System.Exception)
            {
                
                return false;
            }
            return true;
        }
    }
}