using System.Collections.Generic;
using tfweb.entity;
using tfweb.repository;
using tfweb.repository.ViewModel;

namespace tfweb.service.Implementacion
{
    public class CargoService : ICargoService
    {
        private ICargoRepository pro;

        public CargoService (ICargoRepository pro) {
            this.pro= pro;
        }


        public bool Delete(int id)
        {
            return pro.Delete(id);
        }

         public Cargo Get(int id)
        {
             return pro.Get(id);
        }


    public IEnumerable<Cargo> GetAll()
        {
           return pro.GetAll();
        }

     public bool Save(Cargo entity)
        {
               return pro.Save(entity);
        }


        public bool Update(Cargo entity)
        {
           return pro.Update(entity);
        }
    }
}