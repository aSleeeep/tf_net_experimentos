using System.Collections;
using System.Collections.Generic;
using System.Linq;
using tfweb.entity;
using tfweb.repository.Context;

namespace tfweb.repository.Implementacion
{
    public class ProductoRepository : IProductoRepository
    {

          private ApplicationDbContext context;

        public ProductoRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public bool Delete(int id)
        {
            try
            {
            var resutl = context.Productos.Single(x => x.Id == id);
            context.Remove(resutl);
            context.SaveChanges();
            }
            catch (System.Exception)
            {
            return false;
            }
            return true;
        }

        public IEnumerable fetchProductoByName(string texto)
        {
             var result = new List<Producto> ();

            try 
            {
                result = context.Productos.Where(m=> m.Nombre.Contains(texto)).ToList ();
            } 
            catch (System.Exception) {

                throw;
            }
            
            return result;
        }

        public Producto Get(int id)
        {
             var result = new Producto();

            try
            {
            result = context.Productos.Single(x => x.Id == id);
            }
            catch (System.Exception)
            {

            throw;
            }
            return result;
        }

        public IEnumerable<Producto> GetAll()
        {
             var result = new List<Producto>();
            try
            {
                result = context.Productos.ToList();
            }
            catch (System.Exception)
            {

                throw;
            }
            return result;
        }

        public bool Save(Producto entity)
        {
           

            try
            {
               
                  context.Add(entity);
                context.SaveChanges(); 
     
            }
            catch (System.Exception)
            {

                return false;
            }
            return true;
        }

        public bool Update(Producto entity)
        {
             try
            {
                 var pro = 

context.Productos.Single( x => x.Id == entity.Id);

                 pro.Nombre=entity.Nombre;
                 pro.Tipo=entity.Tipo;
                 pro.Precio=entity.Precio;

                 context.Update(pro);
                 context.SaveChanges();
            }
            catch (System.Exception)
            {
                
                return false;
            }
            return true;
        }
    }
}