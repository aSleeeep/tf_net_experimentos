using System.Collections.Generic;
using tfweb.entity;
using tfweb.repository;
using tfweb.repository.ViewModel;

namespace tfweb.service.Implementacion
{
    public class LocalService : ILocalService
    {
         private ILocalRepository pro;

        public LocalService (ILocalRepository pro) 
        {
            this.pro= pro;
        }







       public bool Delete(int id)
        {
            return pro.Delete(id);
        }
      

         public Local Get(int id)
        {
            return pro.Get(id);
        }





public IEnumerable<Local> GetAll()
        {
           return  pro.GetAll();
        }
        public bool Save(Local entity)
        {
               return pro.Save(entity);
        }

        public bool Update(Local entity)
        {
           return pro.Update(entity);
        }
    }
}