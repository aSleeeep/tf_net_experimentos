
using Microsoft.AspNetCore.Mvc;
using tfweb.entity;
using tfweb.service;

namespace webfinal.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocalController : ControllerBase
    {

        private ILocalService pro;

        public LocalController(ILocalService pro)
        {
            this.pro = pro;
        }

        [HttpGet]
        public ActionResult Get()
        {
            return Ok(pro.GetAll());
        }

        [HttpPost]
        public ActionResult Post([FromBody] Local clie)
        {
            return Ok( pro.Save(clie) );
        }

        [HttpPut]
        public ActionResult Put([FromBody] Local clie)
        {
            return Ok( pro.Update(clie) );
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            return Ok(  pro.Delete(id));
        }

    }
}