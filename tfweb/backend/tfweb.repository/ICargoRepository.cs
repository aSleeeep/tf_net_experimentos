using tfweb.entity;

namespace tfweb.repository
{
    public interface ICargoRepository: IRepository<Cargo>
    {
        
    }
}