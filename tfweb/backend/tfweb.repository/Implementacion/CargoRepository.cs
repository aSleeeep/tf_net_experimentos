using System.Collections.Generic;
using System.Linq;
using tfweb.entity;
using tfweb.repository.Context;

namespace tfweb.repository.Implementacion
{
    public class CargoRepository : ICargoRepository
    {

         private ApplicationDbContext context;

        public CargoRepository(ApplicationDbContext context)
        {
            this.context = context;
        }


        public bool Delete(int id)
        {
             try
            {
            var resutl = context.Cargos.Single(x => x.Id == id);
            context.Remove(resutl);
            context.SaveChanges();
            }
            catch (System.Exception)
            {
            return false;
            }
            return true;
        }

        public Cargo Get(int id)
        {
            var result = new Cargo();

            try
            {
            result = context.Cargos.Single(x => x.Id == id);
            }
            catch (System.Exception)
            {

            throw;
            }
            return result;
        }

        public IEnumerable<Cargo> GetAll()
        {
              var result = new List<Cargo>();
            try
            {
                result = context.Cargos.ToList();
            }

            catch (System.Exception)
            {

                throw;
            }
            return result;
        }

        public bool Save(Cargo entity)
        {
           try
            {
                context.Cargos.Add(entity);
                context.SaveChanges();

            }
            catch (System.Exception)
            {

                return false;
            }
            return true;
        }

        public bool Update(Cargo entity)
        {   try
            {
                 var pro = 

context.Cargos.Single( x => x.Id == entity.Id);

                 pro.Nombre=entity.Nombre;
                   pro.Sueldo=entity.Sueldo;

                 context.Update(pro);
                 context.SaveChanges();
            }
            catch (System.Exception)
            {
                
                return false;
            }
            return true;
        }
    }
}