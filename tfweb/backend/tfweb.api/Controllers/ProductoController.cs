
using Microsoft.AspNetCore.Mvc;
using tfweb.entity;
using tfweb.service;

namespace webfinal.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController : ControllerBase
    {

        private IProductoService pro;

        public ProductoController(IProductoService pro)
        {
            this.pro = pro;
        }

        [HttpGet]
        public ActionResult Get()
        {
            return Ok(pro.GetAll());
        }

        [HttpPost]
        public ActionResult Post([FromBody] Producto clie)
        {
            return Ok( pro.Save(clie) );
        }

        [HttpPut]
        public ActionResult Put([FromBody] Producto clie)
        {
            return Ok( pro.Update(clie) );
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            return Ok(  pro.Delete(id));
        }

        [HttpGet ("{texto}")]
        public ActionResult FecthProductoByName ([FromRoute] string texto) {
            return Ok (
                pro.fetchProductoByName(texto)
            );
        }

    }
}