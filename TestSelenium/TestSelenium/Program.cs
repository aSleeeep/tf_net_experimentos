﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Opera;
using System.Threading;

namespace TestSelenium
{
    class Program
    {

    

        static void PruebaManagerChrome()
        {
            ChromeDriverService service = ChromeDriverService.CreateDefaultService(@"C:\chromedriver", "chromedriver.exe");
     
            IWebDriver chrome = new ChromeDriver(service);
            chrome.Navigate().GoToUrl("http://localhost:8080/");
            chrome.Manage().Window.Maximize();


            //-----------------------------Añadir Cliente-----------------------------------------------//
            IWebElement menu_mantenimiento = chrome.FindElement(By.Id("mantenimiento"));
            menu_mantenimiento.Click();

            Thread.Sleep(1500);
            IWebElement menu_clientes = chrome.FindElement(By.Id("clientes"));
            menu_clientes.Click();


            Thread.Sleep(1500);
            IWebElement btn_nuevo_cliente = chrome.FindElement(By.Id("btn-nuevo-cliente"));
            btn_nuevo_cliente.Click();


            Thread.Sleep(1500);
            IWebElement nombre_cliente_añadir = chrome.FindElement(By.Id("nombre_cliente_a_añadir"));
            nombre_cliente_añadir.SendKeys("Bryan");

            Thread.Sleep(1500);
            IWebElement guardar_cliente = chrome.FindElement(By.Id("guardar_cliente"));
            guardar_cliente.Click();

            //-----------------------------------------------------------------------------------------------//

            //-----------------------------Añadir Producto-----------------------------------------------//
            Thread.Sleep(1500);
            IWebElement menu_productos = chrome.FindElement(By.Id("productos"));
            menu_productos.Click();


            Thread.Sleep(1500);
            IWebElement btn_nuevo_producto = chrome.FindElement(By.Id("btn-nuevo-producto"));
            btn_nuevo_producto.Click();

            Thread.Sleep(1500);
            chrome.FindElement(By.Id("nombre-nuevo-producto")).SendKeys("Papas Fritas");
            chrome.FindElement(By.Id("tipo-nuevo-producto")).SendKeys("Comestible");
            chrome.FindElement(By.Id("precio-nuevo-producto")).SendKeys("5");

             Thread.Sleep(1500);
            chrome.FindElement(By.Id("guardar-producto")).Click();

            
            //-----------------------------------------------------------------------------------------------//

            //-----------------------------Añadir Venta-----------------------------------------------//


            Thread.Sleep(1500);
            chrome.FindElement(By.Id("ventas")).Click();

            Thread.Sleep(1500);
            chrome.FindElement(By.Id("btn-nueva-venta")).Click();



            Thread.Sleep(1500);
            chrome.FindElement(By.Id("select-metodo-pago")).SendKeys("Efectivo");
            chrome.FindElement(By.Id("numero-orden")).SendKeys("1");
            chrome.FindElement(By.Id("select-empleado")).SendKeys("Jose");
            chrome.FindElement(By.Id("select-cliente")).SendKeys("Bryan"); 
            chrome.FindElement(By.Id("total")).SendKeys("1");


            Thread.Sleep(1500);
            chrome.FindElement(By.Id("btn-mostrar-productos")).Click();
            
            Thread.Sleep(1500);
            chrome.FindElement(By.Id("agregar-producto-alcarro")).Click();

            Thread.Sleep(1500);
            chrome.FindElement(By.Id("btn-salir-agregar-productos")).Click();

            Thread.Sleep(1500);
            chrome.FindElement(By.Id("btn-guardar-venta")).Click();
            //----------------------------------------------------------------------------------//
            Thread.Sleep(5500);

            chrome.Close();
            
        }
        static void PruebaStaffChrome()
        {
            ChromeDriverService service = ChromeDriverService.CreateDefaultService(@"C:\chromedriver", "chromedriver.exe");

            IWebDriver chrome = new ChromeDriver(service);
            chrome.Navigate().GoToUrl("http://localhost:8080/");
            chrome.Manage().Window.Maximize();

            chrome.FindElement(By.Id("staff")).Click();

            //--------------------------------------Agregar Cargo---------------------------------------------//
            Thread.Sleep(1500);
            chrome.FindElement(By.Id("cargos")).Click();

            Thread.Sleep(1500);
            chrome.FindElement(By.Id("cargos")).Click();

            Thread.Sleep(1500);
            chrome.FindElement(By.Id("btn-nuevo-cargo")).Click();

            Thread.Sleep(1500);
            chrome.FindElement(By.Id("nombre")).SendKeys("Cajero");
            chrome.FindElement(By.Id("sueldo")).SendKeys("500");

            Thread.Sleep(1500);
            chrome.FindElement(By.Id("agregar-cargo")).Click();

            //--------------------------------------Agregar local---------------------------------------------//

            Thread.Sleep(1500);
            chrome.FindElement(By.Id("locales")).Click();

            Thread.Sleep(1500);
            chrome.FindElement(By.Id("btn-nuevo-local")).Click();

            Thread.Sleep(1500);
            chrome.FindElement(By.Id("nombre")).SendKeys("Bembos La Molina");
            chrome.FindElement(By.Id("direccion")).SendKeys("Av. La Molina");
            chrome.FindElement(By.Id("distrito")).SendKeys("La Molina");

            Thread.Sleep(1500);
            chrome.FindElement(By.Id("agregar-local")).Click();

            //--------------------------------------Agregar Empleado---------------------------------------------//

            Thread.Sleep(1500);
            chrome.FindElement(By.Id("empleados")).Click();

            Thread.Sleep(1500);
            chrome.FindElement(By.Id("btn-nuevo-empleado")).Click();

            Thread.Sleep(1500);
            chrome.FindElement(By.Id("local")).SendKeys("1");
            chrome.FindElement(By.Id("cargo")).SendKeys("1");
            chrome.FindElement(By.Id("nombre")).SendKeys("Jose");
            chrome.FindElement(By.Id("apellido")).SendKeys("Galvez");
            chrome.FindElement(By.Id("direccion")).SendKeys("Av. La Marina 123");
            chrome.FindElement(By.Id("celular")).SendKeys("950123563");
            Thread.Sleep(1500);

            chrome.FindElement(By.Id("agregar-empleado")).Click();

            Thread.Sleep(1500);
            chrome.Close();
        }

        static void pruebaStaffOpera()
        {
            OperaDriverService service = OperaDriverService.CreateDefaultService(@"C:\opera", "operadriver.exe");
            var operaOptions = new OperaOptions
            {
                BinaryLocation = "C:\\Users\\Bryan\\AppData\\Local\\Programs\\Opera\\65.0.3467.48\\opera.exe",
                LeaveBrowserRunning = false
            };

            IWebDriver opera = new OperaDriver(service,operaOptions);
            opera.Navigate().GoToUrl("http://localhost:8080/");
            opera.Manage().Window.Maximize();



            opera.FindElement(By.Id("staff")).Click();

            //--------------------------------------Agregar Cargo---------------------------------------------//
            Thread.Sleep(1500);
            opera.FindElement(By.Id("cargos")).Click();

            Thread.Sleep(1500);
            opera.FindElement(By.Id("cargos")).Click();

            Thread.Sleep(1500);
            opera.FindElement(By.Id("btn-nuevo-cargo")).Click();

            Thread.Sleep(1500);
            opera.FindElement(By.Id("nombre")).SendKeys("Cajero");
            opera.FindElement(By.Id("sueldo")).SendKeys("500");

            Thread.Sleep(500);
            opera.FindElement(By.Id("agregar-cargo")).Click();

            //--------------------------------------Agregar local---------------------------------------------//

            Thread.Sleep(2500);
            opera.FindElement(By.Id("locales")).Click();

            Thread.Sleep(1500);
            opera.FindElement(By.Id("btn-nuevo-local")).Click();

            Thread.Sleep(1500);
            opera.FindElement(By.Id("nombre")).SendKeys("Bembos La Molina");
            opera.FindElement(By.Id("direccion")).SendKeys("Av. La Molina");
            opera.FindElement(By.Id("distrito")).SendKeys("La Molina");

            Thread.Sleep(1500);
            opera.FindElement(By.Id("agregar-local")).Click();

            //--------------------------------------Agregar Empleado---------------------------------------------//

            Thread.Sleep(1500);
            opera.FindElement(By.Id("empleados")).Click();

            Thread.Sleep(1500);
            opera.FindElement(By.Id("btn-nuevo-empleado")).Click();

            Thread.Sleep(1500);
            opera.FindElement(By.Id("local")).SendKeys("1");
            opera.FindElement(By.Id("cargo")).SendKeys("1");
            opera.FindElement(By.Id("nombre")).SendKeys("Jose");
            opera.FindElement(By.Id("apellido")).SendKeys("Galvez");
            opera.FindElement(By.Id("direccion")).SendKeys("Av. La Marina 123");
            opera.FindElement(By.Id("celular")).SendKeys("950123563");
            Thread.Sleep(1500);

            opera.FindElement(By.Id("agregar-empleado")).Click();

            Thread.Sleep(1500);
            opera.Close();




        }

        static void pruebaManagerOpera()
        {
            OperaDriverService service = OperaDriverService.CreateDefaultService(@"C:\opera", "operadriver.exe");
            var operaOptions = new OperaOptions
            {
                BinaryLocation = "C:\\Users\\Bryan\\AppData\\Local\\Programs\\Opera\\65.0.3467.48\\opera.exe",
                LeaveBrowserRunning = false
            };

            IWebDriver opera = new OperaDriver(service, operaOptions);
            opera.Navigate().GoToUrl("http://localhost:8080/");
            opera.Manage().Window.Maximize();

            //-----------------------------Añadir Cliente-----------------------------------------------//
            IWebElement menu_mantenimiento = opera.FindElement(By.Id("mantenimiento"));
            menu_mantenimiento.Click();

            Thread.Sleep(1500);
            IWebElement menu_clientes = opera.FindElement(By.Id("clientes"));
            menu_clientes.Click();


            Thread.Sleep(1500);
            IWebElement btn_nuevo_cliente = opera.FindElement(By.Id("btn-nuevo-cliente"));
            btn_nuevo_cliente.Click();


            Thread.Sleep(1500);
            IWebElement nombre_cliente_añadir = opera.FindElement(By.Id("nombre_cliente_a_añadir"));
            nombre_cliente_añadir.SendKeys("Bryan");

            Thread.Sleep(1500);
            IWebElement guardar_cliente = opera.FindElement(By.Id("guardar_cliente"));
            guardar_cliente.Click();

            //-----------------------------------------------------------------------------------------------//

            //-----------------------------Añadir Producto-----------------------------------------------//
            Thread.Sleep(1500);
            IWebElement menu_productos = opera.FindElement(By.Id("productos"));
            menu_productos.Click();


            Thread.Sleep(1500);
            IWebElement btn_nuevo_producto = opera.FindElement(By.Id("btn-nuevo-producto"));
            btn_nuevo_producto.Click();

            Thread.Sleep(1500);
            opera.FindElement(By.Id("nombre-nuevo-producto")).SendKeys("Papas Fritas");
            opera.FindElement(By.Id("tipo-nuevo-producto")).SendKeys("Comestible");
            opera.FindElement(By.Id("precio-nuevo-producto")).SendKeys("5");

            Thread.Sleep(1500);
            opera.FindElement(By.Id("guardar-producto")).Click();


            //-----------------------------------------------------------------------------------------------//

            //-----------------------------Añadir Venta-----------------------------------------------//


            Thread.Sleep(1500);
            opera.FindElement(By.Id("ventas")).Click();

            Thread.Sleep(1500);
            opera.FindElement(By.Id("btn-nueva-venta")).Click();



            Thread.Sleep(1500);
            opera.FindElement(By.Id("select-metodo-pago")).SendKeys("Efectivo");
            opera.FindElement(By.Id("numero-orden")).SendKeys("1");
            opera.FindElement(By.Id("select-empleado")).SendKeys("Jose");
            opera.FindElement(By.Id("select-cliente")).SendKeys("Bryan");
            opera.FindElement(By.Id("total")).SendKeys("1");


            Thread.Sleep(1500);
            opera.FindElement(By.Id("btn-mostrar-productos")).Click();

            Thread.Sleep(1500);
            opera.FindElement(By.Id("agregar-producto-alcarro")).Click();

            Thread.Sleep(1500);
            opera.FindElement(By.Id("btn-salir-agregar-productos")).Click();

            Thread.Sleep(1500);
            opera.FindElement(By.Id("btn-guardar-venta")).Click();
            //----------------------------------------------------------------------------------//
            Thread.Sleep(5500);

            opera.Close();

        }

        static void Main(string[] args)
        {
            pruebaManagerOpera();
        }
    }
}
