namespace tfweb.repository.ViewModel
{
    public class DetalleOrdenViewModel
    {
        

            public int Id { get; set; }
        public int OrdenId { get; set; }
        public int ProductoId { get; set; }
        public string NombreProducto { get; set; }
        public int Cantidad { get; set; }

        public int Precio { get; set; }

    }
}