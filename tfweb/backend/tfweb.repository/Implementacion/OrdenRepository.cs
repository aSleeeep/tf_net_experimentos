using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using tfweb.entity;
using tfweb.repository.Context;
using tfweb.repository.ViewModel;

namespace tfweb.repository.Implementacion
{
    public class OrdenRepository : IOrdenRepository
    {
         private ApplicationDbContext context;

        public OrdenRepository (ApplicationDbContext context) 
        {
            this.context = context;
        }

        public bool Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public Orden Get(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<Orden> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<OrdenViewModel> GetAllOrdenes()
        {
             var orden = context.Ordenes.Include ( o =>  o.Cliente ).Include(o => o.Empleado).Take (10).ToList ();

            return orden.Select 
            (o => new OrdenViewModel
             {
                    Id = o.Id,
                    OrdenNro = o.OrdenNro,
                    EmpleadoId = o.empleadoId,
                    NombreEmpleado= o.Empleado.Nombre,
                    ClienteId=o.ClienteId,
                    NombreCliente=o.Cliente.Nombre,
                   Total = o.Total,
                    PagoMetodo = o.PagoMetodo


            }
            );
        }

        public IEnumerable<DetalleOrdenViewModel> ListarDetalles(int ordenId)
        {
            var detalle = context.DetalleOrdenes
                .Include (m => m.Producto)
                .Where (d => d.OrdenId== ordenId)
                .ToList ();

                  return detalle.Select (d => new DetalleOrdenViewModel
             {
                ProductoId=d.ProductoId,
                    NombreProducto= d.Producto.Nombre,
                    Cantidad = d.Cantidad,
                    Precio = d.Producto.Precio
            });
        }

        public bool Save(Orden entity)
        {
            
            Orden bol = new Orden
            {
               empleadoId = entity.empleadoId,
                ClienteId = entity.ClienteId,
               OrdenNro= entity.OrdenNro,
                PagoMetodo = entity.PagoMetodo,
               Total=entity.Total
            };
            try 
            {
                context.Ordenes.Add (bol);
                context.SaveChanges ();
                var ordenId = bol.Id;
                foreach (var item in entity.DetalleOrden) 
                {
                    DetalleOrden detalle = new DetalleOrden
                    {
                        OrdenId = item.OrdenId,
                        ProductoId= item.ProductoId,
                        Cantidad = item.Cantidad
                    };
                    context.DetalleOrdenes.Add (detalle);
                }
                context.SaveChanges ();
            } 
            catch (Exception ) {
                return false;
            }
            return true;
        }

        public bool Update(Orden entity)
        {
            throw new System.NotImplementedException();
        }
    }
}