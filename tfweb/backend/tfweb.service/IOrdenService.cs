using System.Collections.Generic;
using tfweb.entity;
using tfweb.repository.ViewModel;

namespace tfweb.service
{
    public interface IOrdenService:IService<Orden>
    {
          IEnumerable<OrdenViewModel> GetAllOrdenes ();
        IEnumerable<DetalleOrdenViewModel> ListarDetalles (int ordenId);
    }
}