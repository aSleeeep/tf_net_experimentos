
 using System.ComponentModel.DataAnnotations;
      
namespace tfweb.entity
{
    public class DetalleOrden
    {
//  PK
        public int Id { get; set; }

// FK 
[Required]
        public int Cantidad { get; set; }
[Required]
        public int OrdenId { get; set; }
   
        public Orden Orden { get; set; }

[Required]
        public int ProductoId { get; set; }

        

         public Producto Producto { get; set; }

//  FK
     

    }
}