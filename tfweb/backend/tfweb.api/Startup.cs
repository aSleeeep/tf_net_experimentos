﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using tfweb.repository.Context;
using tfweb.repository;
using tfweb.repository.Implementacion;
using tfweb.service;
using   tfweb.service.Implementacion;
using Swashbuckle.AspNetCore.Swagger;
using webfinal.api;


namespace tfweb.api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
           services.AddDbContext<ApplicationDbContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddTransient<IOrdenRepository, OrdenRepository> ();
           services.AddTransient<IOrdenService, OrdenService> ();

               services.AddTransient<ICargoRepository, CargoRepository> ();
           services.AddTransient<ICargoService, CargoService> ();

               services.AddTransient<IClienteRepository, ClienteRepository> ();
           services.AddTransient<IClienteService, ClienteService> ();

              services.AddTransient<IEmpleadoRepository, EmpleadoRepository> ();
            services.AddTransient<IEmpleadoService, EmpleadoService> ();

            services.AddTransient<ILocalRepository, LocalRepository> ();
            services.AddTransient<ILocalService, LocalService> ();

            services.AddTransient<IProductoRepository, ProductoRepository> ();
            services.AddTransient<IProductoService, ProductoService> ();


            
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

             services.AddSwaggerGen (swagger => 
            {
                var contact = new Contact () 
                {
                     Name = SwaggerConfiguration.ContactName, Url = SwaggerConfiguration.ContactUrl 
                };

                swagger.SwaggerDoc (SwaggerConfiguration.DocNameV1,
                    new Info 
                    {
                        Title = SwaggerConfiguration.DocInfoTitle,
                            Version = SwaggerConfiguration.DocInfoVersion,
                            Description = SwaggerConfiguration.DocInfoDescription,
                            Contact = contact
                    }
                );
            });

            services.AddCors (options => {
                options.AddPolicy ("Todos",
                    builder => builder.WithOrigins ("*").WithHeaders ("*").WithMethods ("*"));
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
           app.UseSwagger ();

            app.UseSwaggerUI (c => {
                c.SwaggerEndpoint (SwaggerConfiguration.EndpointUrl, SwaggerConfiguration.EndpointDescription);
            });


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
               // app.UseHsts();
            }
            app.UseCors ("Todos");

            //app.UseHttpsRedirection();
            app.UseMvc();

        }
    }
}
