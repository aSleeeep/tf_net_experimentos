using System;
using System.Collections.Generic;

namespace tfweb.repository.ViewModel
{
    public class OrdenViewModel
    {
        public int Id { get; set; }
        public string OrdenNro { get; set; }
        public int ClienteId { get; set; }
        public string NombreCliente { get; set; }

         public int EmpleadoId { get; set; }
        public string NombreEmpleado { get; set; }
        public string PagoMetodo { get; set; }
        public int Total { get; set; }

        public IEnumerable<DetalleOrdenViewModel> DetalleOrden { get; set; }
         

     
    }
}