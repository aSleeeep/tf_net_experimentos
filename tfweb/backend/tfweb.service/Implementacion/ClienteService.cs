using System.Collections.Generic;
using tfweb.entity;
using tfweb.repository;
using tfweb.repository.ViewModel;

namespace tfweb.service.Implementacion
{
    public class ClienteService : IClienteService
    {
         private IClienteRepository pro;

        public ClienteService  (IClienteRepository pro) 
        {
            this.pro= pro;
        }


        public bool Delete(int id)
        {
            return pro.Delete(id);
        }
      

        public Cliente Get(int id)
        {
            return pro.Get(id);
        }


        public IEnumerable<Cliente> GetAll()
        {
           return pro.GetAll();
        }

       public bool Save(Cliente entity)
        {
               return pro.Save(entity);
        }
        public bool Update(Cliente entity)
        {
           return pro.Update(entity);
        }
    }
}